package testbed;

import amp.AmpConstants;
import amp.AmpLogging;
import amp.Amplet;
import com.ampex.amperabase.KeyType;
import engine.ASEConstants;
import engine.ASELogging;
import engine.ByteCodeEngine;
import engine.IByteCodeEngine;
import engine.binary.BinaryFactory;
import engine.binary.IBinary;
import engine.data.*;
import engine.data.constant_memory.ConstantMemoryFactory;
import engine.data.constant_memory.on_ice.ConstantMemoryNoGapsComplexHPS;
import engine.data.constant_memory.on_ice.ConstantMemory;
import engine.data.jump_memory.JumpMemoryFactory;
import engine.data.jump_memory.on_ice.JumpMemory;
import engine.data.jump_memory.on_ice.JumpMemoryNoGapsHA;
import engine.data.writable_memory.WritableMemoryFactory;
import engine.exceptions.ASEException;
import engine.program.IProgram;
import engine.program.OPCode;
import engine.program.Program;

import java.util.ArrayList;
import java.util.Random;

/**
 * Ampex Script Engine library created by John Minor.
 */
public class ASETestbed
{
    public static void main(String[] args) throws Exception
    {
        AmpLogging.startLogging();
        ASELogging.startLogging();

        try
        {
            throw new ASEException("WHWWHHHHYYYY");
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        try
        {
            throw new ASEException("WHWWHHHHYYYY");
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        try
        {
            throw new ASEException("WHWWHHHHYYYY");
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        try
        {
            throw new ASEException("WHWWHHHHYYYY");
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        boolean word16 = ASEConstants.WORD16;

        IByteCodeEngine engine = new ByteCodeEngine(1, false);

        engine.addReservedWord8Operators();

        engine.addOperators("engine.operators.test_set");

        engine.finalizeOperators();

        IProgram program = new Program();
        program.addOPCode(new OPCode(1, 54), word16);
        program.addOPCode(new OPCode(1, 54), word16);
        program.addOPCode(new OPCode(0, 55), word16);
        program.addOPCode(new OPCode(0, 52), word16);
        program.addOPCode(new OPCode(0, 7), word16);
        program.addOPCode(new OPCode(0, 7), word16);
        program.addOPCode(new OPCode(0, 55), word16);
        program.addOPCode(new OPCode(50, 54), word16);
        program.addOPCode(new OPCode(0, 57), word16);
        program.addOPCode(new OPCode(0, 0), word16);
        program.addOPCode(new OPCode(0, 0), word16);
        program.addOPCode(new OPCode(0, 0), word16);
        program.addOPCode(new OPCode(0, 1), word16);
        program.addOPCode(new OPCode(0, 0), word16);
        program.addOPCode(new OPCode(0, 0), word16);
        program.addOPCode(new OPCode(0, 0), word16);

        for (int i = 0; i < 8000; i++)
        {
            program.addOPCode(new OPCode(0, 0), word16);
        }

        program.seal();

        byte[] programBytes = program.serializeToBytes();

        program = new Program(programBytes);

        byte[] eightk = new byte[ASEConstants.DATA_ELEMENT_MAX_SIZE];//"What a weird program.".getBytes();

        for (int i = 0; i < ASEConstants.DATA_ELEMENT_MAX_SIZE; i++)
        {
            eightk[i] = 74;
        }

        System.out.println(new String(eightk, AmpConstants.UTF8));
        //new Random().nextBytes(eightk);

        IDataElement goi = new DataElement(eightk);
        IDataElement[] arry = new IDataElement[ASEConstants.CONSTANT_MEMORY_MAX_DATA_ELEMENTS];

        for (int i = 0; i < ASEConstants.CONSTANT_MEMORY_MAX_DATA_ELEMENTS; i++)
        {
            arry[i] = goi;
        }

        IConstantMemory constantMemory = ConstantMemoryFactory.build(arry);

        boolean word8 = false;

        int version = 1;

        byte[] entropy = new byte[ASEConstants.ENTROPY_MAX_MAX_LENGTH];
        new Random().nextBytes(entropy);

        long timestamp = System.currentTimeMillis();

        byte[] publicKey = new byte[ASEConstants.PUBLIC_KEY_MAX_LENGTH];
        new Random().nextBytes(publicKey);

        IJumpMemory jumpMemory = JumpMemoryFactory.build();

        byte[] feeAddress = new byte[ASEConstants.FEE_ADDRESS_MAX_SIZE];
        new Random().nextBytes(feeAddress);

        feeAddress = null;

        int feeMult = 5;

        IBinary bin = BinaryFactory.build(program, constantMemory, jumpMemory, word8, version, entropy, timestamp, publicKey, KeyType.ED25519, feeAddress, feeMult);

        Amplet binAmp = bin.serializeToAmplet();

        byte[] binAmpBytes = binAmp.serializeToBytes();

        //byte[] binJSONBytes = bin.serializeToJSON().getBytes();

        bin = BinaryFactory.buildFromAmplet(binAmp);

        if (bin == null)
        {
            return;
        }


        byte[] feeAddress2 = bin.getFeeAddress();
        long feeMult2 = bin.getFeeMultiplier();

        IJumpMemory jumpjump = bin.getJumpMemory();

        byte[] entropo = bin.getEntropy();

        long timestoomp = bin.getTimestamp();

        byte[] publook = bin.getPublicKey();

        IConstantMemory contster = bin.getConstantMemory();

        byte[] ampbytes = contster.serializeToBytes();

        //String jsonstring = contster.serializeToJSON();

        //System.out.println(jsonstring);

        //byte[] jsonbytes = jsonstring.getBytes();

        IWritableMemory writableMemory = WritableMemoryFactory.build();

        byte[] wmbytes = writableMemory.serializeToBytes();

        writableMemory = WritableMemoryFactory.build(wmbytes);

        IDataElement thing = new DataElement("Ho.".getBytes(AmpConstants.UTF8));
        writableMemory.setElement(thing, 0);

        IDataElement thing1 = new DataElement("Hi!".getBytes(AmpConstants.UTF8));
        writableMemory.setElement(thing1, 1);

        IDataElement thing2 = new DataElement("Hihi!".getBytes(AmpConstants.UTF8));
        writableMemory.setElement(thing2, 2);

        byte[] wmBytes = writableMemory.serializeToBytes();

        writableMemory = WritableMemoryFactory.build(wmBytes);

        byte[] executionAddress = new byte[10];

        byte[] pib = bin.getProgramIdentityBytes();

        engine.executeProgram(bin, writableMemory, null, executionAddress, false);
        engine.executeProgram(bin, writableMemory, null, executionAddress, false);
        engine.executeProgram(bin, writableMemory, null, executionAddress, false);

        ArrayList<IDataElement> result = null;

        long startTime = System.nanoTime();

        for (int i = 0; i < 400_000; i++)
        {
            result = engine.executeProgram(bin, writableMemory, null, executionAddress, false);
        }

        long endTime = System.nanoTime() - startTime;

        System.out.println("\nTook " + endTime + " to complete.");

        for (IDataElement element : result)
        {
            System.out.println("\nElement data in various forms:");
            System.out.println(element.getDataAsString());
            System.out.println(element.getDataAsBigInteger().longValue());
        }


        short[] nogap = new short[ASEConstants.JUMP_MEMORY_MAX_ELEMENTS];
        nogap[0] = 5;
        nogap[1] = 10;
        nogap[2] = 16;

        short[] gap = new short[ASEConstants.JUMP_MEMORY_MAX_ELEMENTS];
        for (int i = 0; i < ASEConstants.JUMP_MEMORY_MAX_ELEMENTS; i++)
        {
            gap[i] = -1;
        }
        gap[0] = 5;
        gap[1] = 10;
        gap[2] = 16;

        gap[10] = 5;

        IJumpMemory jumpLegacyHPA = new JumpMemory(nogap);
        IJumpMemory jumpNoGapHA = new JumpMemoryNoGapsHA(gap);

        byte[] jumpLegacyHPABytes = jumpLegacyHPA.serializeToBytes();
        byte[] jumpNoGapHABytes = jumpNoGapHA.serializeToBytes();

        //jumpLegacyHPA = JumpMemory.deserializeFromBytes(jumpNoGapHABytes);
        jumpNoGapHA = JumpMemoryNoGapsHA.deserializeFromBytes(jumpLegacyHPABytes);

        IJumpMemory deserializeHPA = JumpMemoryFactory.build(jumpLegacyHPABytes);
        IJumpMemory deserializeHA = JumpMemoryFactory.build(jumpNoGapHABytes);


        IConstantMemory memoryHCPA = new ConstantMemoryNoGapsComplexHPS(arry);
        IConstantMemory memoryLegacy = new ConstantMemory(arry);

        byte[] hcpaBytes = memoryHCPA.serializeToBytes();
        byte[] legacyBytes = memoryLegacy.serializeToBytes();

        memoryHCPA = ConstantMemoryNoGapsComplexHPS.deserializeFromBytes(legacyBytes);
        memoryLegacy = ConstantMemory.deserializeFromBytes(hcpaBytes);

        int a = 0;

        memoryHCPA = ConstantMemoryFactory.build(hcpaBytes);
        memoryLegacy = ConstantMemoryFactory.build(legacyBytes);

        a = 1;
    }
}
