package engine;

/**
 * Created by Queue on 3/7/2018.
 */
public class ASEConstants
{
    public static final int DATA_ELEMENT_MAX_SIZE = 192;
    public static final int STACK_MAX_DEPTH = 256;
    public static final int MAX_OPCODES = 256;
    public static final int WRITABLE_MEMORY_MAX_DATA_ELEMENTS = 32;
    public static final int CONSTANT_MEMORY_MAX_DATA_ELEMENTS = 32;
    public static final int JUMP_MEMORY_MAX_ELEMENTS = 16;
    public static final int PROGRAM_MAX_SIZE = 1 * 1024;

    public static final int ENTROPY_MAX_MAX_LENGTH = 128;
    public static final int PUBLIC_KEY_MAX_LENGTH = 192;
    public static final int FEE_ADDRESS_MAX_SIZE = 128;

    public static final boolean WORD8 = true;
    public static final boolean WORD16 = false;


    public static final int RESERVED_OPCODES = 50;

}
