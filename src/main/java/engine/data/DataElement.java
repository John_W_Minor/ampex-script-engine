package engine.data;

import amp.AmpConstants;
import amp.ByteTools;
import engine.ASEConstants;
import engine.exceptions.ASEException;

import java.math.BigInteger;

public class DataElement implements IDataElement
{
    //region Member Fields
    private byte[] bytes = null;
    private int size = 0;
    //endregion

    public DataElement(byte[] _data) throws Exception
    {
        if (_data == null)
        {
            throw new ASEException("Null _data argument.");
        }

        if (_data.length > ASEConstants.DATA_ELEMENT_MAX_SIZE)
        {
            throw new ASEException("Size of _data too large.");
        }

        bytes = _data.clone();
        size = bytes.length;
    }

    public DataElement(short _data) throws Exception
    {
        bytes = ByteTools.deconstructShort(_data);
        size = bytes.length;
    }

    public DataElement(char _data) throws Exception
    {
        bytes = ByteTools.deconstructChar(_data);
        size = bytes.length;
    }

    public DataElement(int _data) throws Exception
    {
        bytes = ByteTools.deconstructInt(_data);
        size = bytes.length;
    }

    public DataElement(float _data) throws Exception
    {
        bytes = ByteTools.deconstructFloat(_data);
        size = bytes.length;
    }

    public DataElement(long _data) throws Exception
    {
        bytes = ByteTools.deconstructLong(_data);
        size = bytes.length;
    }

    public DataElement(double _data) throws Exception
    {
        bytes = ByteTools.deconstructDouble(_data);
        size = bytes.length;
    }

    public DataElement(String _data) throws Exception
    {
        if (_data == null)
        {
            throw new ASEException("Null _data argument.");
        }

        bytes = _data.getBytes(AmpConstants.UTF8);

        if (bytes.length > ASEConstants.DATA_ELEMENT_MAX_SIZE)
        {
            throw new ASEException("Size of _data too large.");
        }

        size = bytes.length;
    }

    public DataElement(BigInteger _data) throws Exception
    {
        if (_data == null)
        {
            throw new ASEException("Null _data argument.");
        }

        bytes = _data.toByteArray();

        if (bytes.length > ASEConstants.DATA_ELEMENT_MAX_SIZE)
        {
            throw new ASEException("Size of _data too large.");
        }

        size = bytes.length;
    }

    //region Getters
    @Override
    public byte[] getData()
    {
        return bytes.clone();
    }

    @Override
    public Integer getDataAsUnsignedByte()
    {
        if (size == AmpConstants.BYTE_BYTE_FOOTPRINT)
        {
            return ByteTools.buildUnsignedByte(bytes[0]);
        }
        return null;
    }

    @Override
    public Byte getDataAsByte()
    {
        if (size == AmpConstants.BYTE_BYTE_FOOTPRINT)
        {
            return bytes[0];
        }
        return null;
    }

    @Override
    public Boolean getDataAsBoolean()
    {
        if (size == AmpConstants.BYTE_BYTE_FOOTPRINT)
        {
            return ByteTools.buildBoolean(bytes[0]);
        }
        return null;
    }

    @Override
    public Short getDataAsShort()
    {
        if (size == AmpConstants.SHORT_BYTE_FOOTPRINT)
        {
            return ByteTools.buildShort(bytes[0], bytes[1]);
        }
        return null;
    }

    @Override
    public Character getDataAsChar()
    {
        if (size == AmpConstants.CHAR_BYTE_FOOTPRINT)
        {
            return ByteTools.buildChar(bytes[0], bytes[1]);
        }
        return null;
    }

    @Override
    public Long getDataAsUnsignedInt()
    {
        if (size == AmpConstants.INT_BYTE_FOOTPRINT)
        {
            return ByteTools.buildUnsignedInt(bytes[0], bytes[1], bytes[2], bytes[3]);
        }
        return null;
    }

    @Override
    public Integer getDataAsInt()
    {
        if (size == AmpConstants.INT_BYTE_FOOTPRINT)
        {
            return ByteTools.buildInt(bytes[0], bytes[1], bytes[2], bytes[3]);
        }
        return null;
    }

    @Override
    public Float getDataAsFloat()
    {
        if (size == AmpConstants.FLOAT_BYTE_FOOTPRINT)
        {
            return ByteTools.buildFloat(bytes[0], bytes[1], bytes[2], bytes[3]);
        }
        return null;
    }

    @Override
    public Long getDataAsLong()
    {
        if (size == AmpConstants.LONG_BYTE_FOOTPRINT)
        {
            return ByteTools.buildLong(bytes[0], bytes[1], bytes[2], bytes[3], bytes[4], bytes[5], bytes[6], bytes[7]);
        }
        return null;
    }

    @Override
    public Double getDataAsDouble()
    {
        if (size == AmpConstants.DOUBLE_BYTE_FOOTPRINT)
        {
            return ByteTools.buildDouble(bytes[0], bytes[1], bytes[2], bytes[3], bytes[4], bytes[5], bytes[6], bytes[7]);
        }
        return null;
    }

    @Override
    public String getDataAsString()
    {
        return new String(bytes, AmpConstants.UTF8);
    }

    @Override
    public String getDataAsHexString()
    {
        return ByteTools.buildHexString(bytes);
    }

    @Override
    public BigInteger getDataAsBigInteger()
    {
        return new BigInteger(bytes);
    }

    @Override
    public int getSize()
    {
        return size;
    }
    //endregion

    @Override
    public byte[] serializeToBytes()
    {
        return bytes.clone();
    }
}
