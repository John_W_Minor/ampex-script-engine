package engine.data.writable_memory;

import amp.typed_data.ITypedData;
import engine.data.IDataElement;
import engine.data.IWritableMemory;
import engine.data.writable_memory.on_ice.WritableMemoryNoGapsComplexHPS;
import engine.data.writable_memory.on_ice.WritableMemory;

public class WritableMemoryFactory
{
    public static IWritableMemory build()
    {
        return new WritableMemoryNoGapsComplexHPS();
    }

    public static IWritableMemory build(IDataElement[] _dataElements) throws Exception
    {
        return new WritableMemoryNoGapsComplexHPS(_dataElements);
    }

    public static IWritableMemory build(ITypedData _data)
    {
        try
        {
            IWritableMemory writableMemory = WritableMemoryNoGapsComplexHPS.deserializeFromITypedData(_data);

            if (writableMemory == null)
            {
                writableMemory = WritableMemory.deserializeFromITypedData(_data);
            }

            return writableMemory;
        } catch (Exception e)
        {
            return null;
        }
    }

    public static IWritableMemory build(byte[] _bytes)
    {
        try
        {
            IWritableMemory writableMemory = WritableMemoryNoGapsComplexHPS.deserializeFromBytes(_bytes);

            if (writableMemory == null)
            {
                writableMemory = WritableMemory.deserializeFromBytes(_bytes);
            }

            return writableMemory;
        } catch (Exception e)
        {
            return null;
        }
    }
}
