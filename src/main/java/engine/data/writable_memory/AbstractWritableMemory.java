package engine.data.writable_memory;

import engine.ASEConstants;
import engine.data.DataElement;
import engine.data.IDataElement;
import engine.data.IWritableMemory;
import engine.exceptions.ASEException;

public abstract class AbstractWritableMemory implements IWritableMemory
{
    //region Member Fields
    protected IDataElement[] dataElements = null;
    //endregion

    //region Constructors
    public AbstractWritableMemory()
    {
        dataElements = new IDataElement[ASEConstants.WRITABLE_MEMORY_MAX_DATA_ELEMENTS];
    }

    public AbstractWritableMemory(IDataElement[] _dataElements) throws Exception
    {
        if (_dataElements == null)
        {
            throw new ASEException("Null _dataElements Argument");
        }

        if (_dataElements.length != ASEConstants.WRITABLE_MEMORY_MAX_DATA_ELEMENTS)
        {
            throw new ASEException("Wrong number of data elements in _dataElements Argument.");
        }

        dataElements = _dataElements.clone();
    }
    //endregion

    //region Getters
    @Override
    public IDataElement getElement(int _address) throws Exception
    {
        if (_address < 0 || _address >= dataElements.length)
        {
            throw new ASEException("Attempted to read invalid writable memory address: " + _address);
        }
        return dataElements[_address];
    }

    @Override
    public int getElementCount()
    {
        return dataElements.length;
    }

    @Override
    public IDataElement[] getAllDataElements()
    {
        IDataElement[] elementsArray = new IDataElement[ASEConstants.WRITABLE_MEMORY_MAX_DATA_ELEMENTS];

        System.arraycopy(dataElements, 0, elementsArray, 0, ASEConstants.WRITABLE_MEMORY_MAX_DATA_ELEMENTS);

        return elementsArray;
    }
    //endregion

    //region Setters
    @Override
    public boolean setElement(IDataElement _element, int _address) throws Exception
    {
        if (_address < 0 || _address >= dataElements.length)
        {
            throw new ASEException("Attempted to write to invalid writable memory address: " + _address);
        }

        dataElements[_address] = _element;
        return true;
    }

    @Override
    public boolean setElement(byte[] _element, int _address) throws Exception
    {
        return setElement(new DataElement(_element), _address);
    }
    //endregion
}