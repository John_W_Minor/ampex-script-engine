package engine.data.constant_memory;

import engine.ASEConstants;
import engine.data.IConstantMemory;
import engine.data.IDataElement;
import engine.exceptions.ASEException;

public abstract class AbstractConstantMemory implements IConstantMemory
{
    //region Member Fields
    protected IDataElement[] dataElements = null;
    //endregion

    //region Constructors
    public AbstractConstantMemory()
    {
        dataElements = new IDataElement[ASEConstants.CONSTANT_MEMORY_MAX_DATA_ELEMENTS];
    }

    public AbstractConstantMemory(IDataElement[] _dataElements) throws Exception
    {
        if (_dataElements == null)
        {
            throw new ASEException("Null _dataElements Argument");
        }

        if (_dataElements.length != ASEConstants.CONSTANT_MEMORY_MAX_DATA_ELEMENTS)
        {
            throw new ASEException("Wrong number of data elements in _dataElements Argument.");
        }

        dataElements = _dataElements.clone();
    }
    //endregion

    //region Getters
    @Override
    public int getElementCount()
    {
        return ASEConstants.CONSTANT_MEMORY_MAX_DATA_ELEMENTS;
    }

    @Override
    public IDataElement[] getAllDataElements()
    {
        return dataElements.clone();
    }

    @Override
    public IDataElement getElement(int _address) throws Exception
    {
        if (_address < 0 || _address >= dataElements.length)
        {
            throw new ASEException("Attempted to read invalid constant memory address: " + _address);
        }
        return dataElements[_address];
    }
    //endregion
}
