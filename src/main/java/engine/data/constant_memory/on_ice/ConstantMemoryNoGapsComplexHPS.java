package engine.data.constant_memory.on_ice;

import amp.headless_amplet.IHeadlessAmpletTwinReadable;
import amp.headless_prefixing_strategies.IHeadlessPrefixingStrategyReadable;
import amp.headless_prefixing_strategies.IHeadlessPrefixingStrategyWritable;
import amp.headless_prefixing_strategies.factory.DefaultHPSFactory;
import amp.headless_prefixing_strategies.strategies.complex_hps.ComplexHPSWritable;
import amp.typed_data.ITypedData;
import amp.typed_data.defaults.DefaultDataTypeEnum;
import engine.ASEConstants;
import engine.ASELogging;
import engine.data.DataElement;
import engine.data.IConstantMemory;
import engine.data.IDataElement;
import engine.data.constant_memory.AbstractConstantMemory;
import logging.IAmpexLogger;

public class ConstantMemoryNoGapsComplexHPS extends AbstractConstantMemory
{
    public static final byte constantMemoryVersion = 2;

    //region Constructors
    public ConstantMemoryNoGapsComplexHPS()
    {
        super();
    }

    public ConstantMemoryNoGapsComplexHPS(IDataElement[] _dataElements) throws Exception
    {
        super(_dataElements);
    }
    //endregion

    //region Serialization
    @Override
    public byte[] serializeToBytes()
    {
        IHeadlessPrefixingStrategyWritable memoryHCPA = ComplexHPSWritable.create();

        memoryHCPA.addElement(constantMemoryVersion);

        for (int i = 0; i < ASEConstants.CONSTANT_MEMORY_MAX_DATA_ELEMENTS; i++)
        {
            IDataElement element = dataElements[i];

            if (element == null || element.getSize() == 0)
            {
                break;
            } else
            {
                memoryHCPA.addElement(element);
            }
        }

        return memoryHCPA.serializeToBytes();
    }

    public static IConstantMemory deserializeFromITypedData(ITypedData _data)
    {
        IAmpexLogger logger = ASELogging.getLogger();

        if (_data == null)
        {
            if (logger != null)
            {
                logger.warn("\nData null when deserializing Constant Memory.");
            }
            return null;
        }

        IHeadlessPrefixingStrategyReadable strat = _data.getDataAsIHeadlessPrefixingStrategyReadable();

        return deserializeFromIHeadlessPrefixingStrategy(strat);
    }

    public static IConstantMemory deserializeFromBytes(byte[] _bytes)
    {
        IAmpexLogger logger = ASELogging.getLogger();

        if (_bytes == null)
        {
            if (logger != null)
            {
                logger.warn("\nBytes null when deserializing Constant Memory.");
            }
            return null;
        }

        IHeadlessPrefixingStrategyReadable strat = DefaultHPSFactory.staticBuild(_bytes);

        return deserializeFromIHeadlessPrefixingStrategy(strat);
    }

    private static IConstantMemory deserializeFromIHeadlessPrefixingStrategy(IHeadlessPrefixingStrategyReadable _strat)
        {
        if(_strat == null)
        {
            return null;
        }

        ITypedData versionHCPA = _strat.getNextElement();

        if (versionHCPA == null)
        {
            return null;
        }

        IHeadlessAmpletTwinReadable versionHA = versionHCPA.getDataAsHeadlessAmpletTwinReadable();

        if (versionHA == null)
        {
            return null;
        }

        Byte version = versionHA.getNextByte();

        if (version == null || version != constantMemoryVersion)
        {
            return null;
        }

        IDataElement[] elements = new IDataElement[ASEConstants.WRITABLE_MEMORY_MAX_DATA_ELEMENTS];

        for (int i = 0; i < ASEConstants.WRITABLE_MEMORY_MAX_DATA_ELEMENTS; i++)
        {
            ITypedData data = _strat.getNextElement();

            if (data == null || data.getType() != DefaultDataTypeEnum.COMPLEX_TYPE)
            {
                break;
            }

            try
            {
                elements[i] = new DataElement(data.getData());
            } catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }

        try
        {
            return new ConstantMemoryNoGapsComplexHPS(elements);
        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
    //endregion
}
