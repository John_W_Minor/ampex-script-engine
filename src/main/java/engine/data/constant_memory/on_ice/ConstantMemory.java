package engine.data.constant_memory.on_ice;

import amp.headless_prefixing_strategies.IHeadlessPrefixingStrategyReadable;
import amp.headless_prefixing_strategies.IHeadlessPrefixingStrategyWritable;
import amp.headless_prefixing_strategies.factory.DefaultHPSFactory;
import amp.headless_prefixing_strategies.strategies.simple_hps.SimpleHPSWritable;
import amp.typed_data.ITypedData;
import engine.ASEConstants;
import engine.ASELogging;
import engine.data.DataElement;
import engine.data.IConstantMemory;
import engine.data.IDataElement;
import engine.data.constant_memory.AbstractConstantMemory;
import logging.IAmpexLogger;

public class ConstantMemory extends AbstractConstantMemory
{

    //region Constructors
    public ConstantMemory()
    {
        super();
    }

    public ConstantMemory(IDataElement[] _dataElements) throws Exception
    {
        super(_dataElements);
    }
    //endregion

    //region Serialization
    @Override
    public byte[] serializeToBytes()
    {
        IHeadlessPrefixingStrategyWritable memoryHPAmplet = SimpleHPSWritable.create();

        byte[] zeroElements = new byte[0];

        memoryHPAmplet.addBytes(zeroElements);

        for (int i = 0; i < ASEConstants.CONSTANT_MEMORY_MAX_DATA_ELEMENTS; i++)
        {
            IDataElement element = dataElements[i];

            if (element == null || element.getSize() == 0)
            {
                break;
            } else
            {
                memoryHPAmplet.addElement(element);
            }
        }

        return memoryHPAmplet.serializeToBytes();
    }

    public static IConstantMemory deserializeFromITypedData(ITypedData _data)
    {
        IAmpexLogger logger = ASELogging.getLogger();

        if (_data == null)
        {
            if (logger != null)
            {
                logger.warn("\nData null when deserializing Constant Memory.");
            }
            return null;
        }

        IHeadlessPrefixingStrategyReadable strat = _data.getDataAsIHeadlessPrefixingStrategyReadable();

        return deserializeFromIHeadlessPrefixingStrategy(strat);
    }

    public static IConstantMemory deserializeFromBytes(byte[] _bytes)
    {
        IAmpexLogger logger = ASELogging.getLogger();

        if (_bytes == null)
        {
            if (logger != null)
            {
                logger.warn("\nBytes null when deserializing Constant Memory.");
            }
            return null;
        }

        IHeadlessPrefixingStrategyReadable strat = DefaultHPSFactory.staticBuild(_bytes);

        return deserializeFromIHeadlessPrefixingStrategy(strat);
    }

    private static IConstantMemory deserializeFromIHeadlessPrefixingStrategy(IHeadlessPrefixingStrategyReadable _strat)
    {
        if(_strat == null)
        {
            return null;
        }

        {
            ITypedData element = _strat.getNextElement();

            byte[] bytes = element.getData();

            if(bytes == null || bytes.length != 0)
            {
                return null;
            }
        }

        IDataElement[] elements = new IDataElement[ASEConstants.CONSTANT_MEMORY_MAX_DATA_ELEMENTS];

        for (int i = 0; i < ASEConstants.CONSTANT_MEMORY_MAX_DATA_ELEMENTS; i++)
        {
            ITypedData element = _strat.getNextElement();

            if (element == null)
            {
                break;
            }

            byte[] bytes = element.getData();

            if(bytes == null)
            {
                break;
            }

            try
            {
                elements[i] = new DataElement(bytes);
            } catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }

        try
        {
            return new ConstantMemory(elements);
        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
    //endregion
}
