package engine.data.constant_memory;

import amp.typed_data.ITypedData;
import engine.data.IConstantMemory;
import engine.data.IDataElement;
import engine.data.constant_memory.on_ice.ConstantMemoryNoGapsComplexHPS;
import engine.data.constant_memory.on_ice.ConstantMemory;

public class ConstantMemoryFactory
{
    public static IConstantMemory build()
    {
        return new ConstantMemoryNoGapsComplexHPS();
    }

    public static IConstantMemory build(IDataElement[] _dataElements) throws Exception
    {
        return new ConstantMemoryNoGapsComplexHPS(_dataElements);
    }

    public static IConstantMemory build(ITypedData _data)
    {
        try
        {
            IConstantMemory constantMemory = ConstantMemoryNoGapsComplexHPS.deserializeFromITypedData(_data);

            if (constantMemory == null)
            {
                constantMemory = ConstantMemory.deserializeFromITypedData(_data);
            }

            return constantMemory;
        } catch (Exception e)
        {
            return null;
        }
    }

    public static IConstantMemory build(byte[] _bytes)
    {
        try
        {
            IConstantMemory constantMemory = ConstantMemoryNoGapsComplexHPS.deserializeFromBytes(_bytes);

            if (constantMemory == null)
            {
                constantMemory = ConstantMemory.deserializeFromBytes(_bytes);
            }

            return constantMemory;
        } catch (Exception e)
        {
            return null;
        }
    }
}
