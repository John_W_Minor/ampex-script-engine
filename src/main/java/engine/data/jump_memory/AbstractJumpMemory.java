package engine.data.jump_memory;

import amp.headless_prefixing_strategies.IHeadlessPrefixingStrategyWritable;
import amp.headless_prefixing_strategies.strategies.simple_hps.SimpleHPSWritable;
import engine.ASEConstants;
import engine.data.IJumpMemory;
import engine.exceptions.ASEException;

public abstract class AbstractJumpMemory implements IJumpMemory
{
    //region Member Fields
    protected short[] jumpPoints = new short[ASEConstants.JUMP_MEMORY_MAX_ELEMENTS];
    //endregion

    //region Constructors
    public AbstractJumpMemory()
    {
    }

    public AbstractJumpMemory(short[] _jumpPoints) throws Exception
    {
        if (_jumpPoints == null)
        {
            throw new ASEException("Null _jumpPoints Argument");
        }

        if (_jumpPoints.length != ASEConstants.JUMP_MEMORY_MAX_ELEMENTS)
        {
            throw new ASEException("Wrong number of jump points in _jumpPoints Argument.");
        }

        jumpPoints = _jumpPoints.clone();
    }
    //endregion

    //region Getters
    @Override
    public int getJumpPointCount()
    {
        return ASEConstants.JUMP_MEMORY_MAX_ELEMENTS;
    }

    @Override
    public short[] getAllJumpPoints()
    {
        short[] jumpsArray = new short[ASEConstants.JUMP_MEMORY_MAX_ELEMENTS];

        System.arraycopy(jumpsArray,0,jumpPoints,0,ASEConstants.JUMP_MEMORY_MAX_ELEMENTS);

        return jumpsArray;
    }

    @Override
    public Short getJumpPoint(int _address) throws Exception
    {
        if (_address < 0 || _address >= jumpPoints.length)
        {
            throw new ASEException("Attempted to read invalid jump memory address: " + _address);
        }
        return jumpPoints[_address];
    }

    @Override
    public byte[] getIdentityBytes()
    {
        IHeadlessPrefixingStrategyWritable memoryHPAmplet = SimpleHPSWritable.create();

        for (int i = 0; i < ASEConstants.JUMP_MEMORY_MAX_ELEMENTS; i++)
        {
            memoryHPAmplet.addElement(jumpPoints[i]);
        }

        return memoryHPAmplet.serializeToBytes();
    }
    //endregion
}
