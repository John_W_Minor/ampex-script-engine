package engine.data.jump_memory;

import engine.data.IJumpMemory;
import engine.data.jump_memory.on_ice.JumpMemory;
import engine.data.jump_memory.on_ice.JumpMemoryNoGapsHA;

public class JumpMemoryFactory
{
    public static IJumpMemory build()
    {
        return new JumpMemoryNoGapsHA();
    }

    public static IJumpMemory build(short[] _jumpPoints) throws Exception
    {
        return new JumpMemoryNoGapsHA(_jumpPoints);
    }

    public static IJumpMemory build(byte[] _bytes)
    {
        try
        {
            IJumpMemory jumpMemory = JumpMemoryNoGapsHA.deserializeFromBytes(_bytes);

            if(jumpMemory == null)
            {
                jumpMemory = JumpMemory.deserializeFromBytes(_bytes);
            }

            return jumpMemory;
        }catch (Exception e)
        {
            return null;
        }
    }
}
