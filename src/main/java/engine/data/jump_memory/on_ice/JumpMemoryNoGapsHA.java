package engine.data.jump_memory.on_ice;

import amp.AmpConstants;
import amp.headless_amplet.HeadlessAmpletTwinReadable;
import amp.headless_amplet.HeadlessAmpletWritable;
import amp.headless_amplet.IHeadlessAmpletTwinReadable;
import amp.headless_amplet.IHeadlessAmpletWritable;
import engine.ASEConstants;
import engine.ASELogging;
import engine.data.jump_memory.AbstractJumpMemory;
import logging.IAmpexLogger;

public class JumpMemoryNoGapsHA extends AbstractJumpMemory
{
    public static final byte jumpMemoryVersion = 2 * -1;

    //region Constructors
    public JumpMemoryNoGapsHA()
    {
        super();
    }

    public JumpMemoryNoGapsHA(short[] _jumpPoints) throws Exception
    {
        super(_jumpPoints);
    }
    //endregion

    //region Serialization
    @Override
    public byte[] serializeToBytes()
    {
        IHeadlessAmpletWritable hamplet = HeadlessAmpletWritable.create();

        hamplet.addElement(jumpMemoryVersion);

        for (short jumpPoint : jumpPoints)
        {
            if (jumpPoint == -1)
            {
                break;
            }

            hamplet.addElement(jumpPoint);
        }

        return hamplet.serializeToBytes();
    }

    public static JumpMemoryNoGapsHA deserializeFromBytes(byte[] _bytes)
    {
        IAmpexLogger logger = ASELogging.getLogger();

        if (_bytes == null)
        {
            if (logger != null)
            {
                logger.warn("\nBytes null when deserializing Jump Memory.");
            }
            return null;
        }

        int length = _bytes.length;

        if(length > (AmpConstants.BYTE_BYTE_FOOTPRINT + ASEConstants.JUMP_MEMORY_MAX_ELEMENTS * AmpConstants.SHORT_BYTE_FOOTPRINT))
        {
            return null;
        }

        IHeadlessAmpletTwinReadable hamplet = HeadlessAmpletTwinReadable.create(_bytes);

        Byte version = hamplet.getNextByte();

        if (version == null || version != jumpMemoryVersion)
        {
            return null;
        }

        short[] jumpPoints = new short[ASEConstants.JUMP_MEMORY_MAX_ELEMENTS];

        for (int i = 0; i < ASEConstants.JUMP_MEMORY_MAX_ELEMENTS; i++)
        {
            jumpPoints[i] = -1;
        }

        for (int i = 0; i < ASEConstants.JUMP_MEMORY_MAX_ELEMENTS; i++)
        {
            Short tempJumpPoint = hamplet.getNextShort();

            if (tempJumpPoint == null)
            {
                break;
            }

            jumpPoints[i] = tempJumpPoint;
        }

        try
        {
            return new JumpMemoryNoGapsHA(jumpPoints);
        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
    //endregion
}
