package engine.data.jump_memory.on_ice;

import amp.headless_amplet.IHeadlessAmpletTwinReadable;
import amp.headless_prefixing_strategies.IHeadlessPrefixingStrategyReadable;
import amp.headless_prefixing_strategies.factory.DefaultHPSFactory;
import amp.typed_data.ITypedData;
import engine.ASEConstants;
import engine.ASELogging;
import engine.data.jump_memory.AbstractJumpMemory;
import logging.IAmpexLogger;

public class JumpMemory extends AbstractJumpMemory
{
    public static final byte jumpMemoryVersion = 1 * -1;

    //region Constructors
    public JumpMemory()
    {
        super();
    }

    public JumpMemory(short[] _jumpPoints) throws Exception
    {
        super(_jumpPoints);
    }
    //endregion

    //region Serialization
    @Override
    public byte[] serializeToBytes()
    {
        return super.getIdentityBytes();
    }

    public static JumpMemory deserializeFromBytes(byte[] _bytes)
    {
        IAmpexLogger logger = ASELogging.getLogger();

        if (_bytes == null)
        {
            if(logger != null)
            {
                logger.warn("\nBytes null when deserializing Jump Memory.");
            }
            return null;
        }

        IHeadlessPrefixingStrategyReadable hpamplet = DefaultHPSFactory.staticBuild(_bytes);

        if(hpamplet == null || !hpamplet.hasNextElement())
        {
            return null;
        }

        short[] jumpPoints = new short[ASEConstants.JUMP_MEMORY_MAX_ELEMENTS];

        for (int i = 0; i < ASEConstants.JUMP_MEMORY_MAX_ELEMENTS; i++)
        {
            ITypedData element = hpamplet.getNextElement();

            if(element == null)
            {
                return null;
            }

            IHeadlessAmpletTwinReadable jumpPointHamplet = element.getDataAsHeadlessAmpletTwinReadable();

            if (jumpPointHamplet == null)
            {
                return null;
            }

            Short tempJumpPoint = jumpPointHamplet.getNextShort();

            if (tempJumpPoint == null)
            {
                return null;
            }

            jumpPoints[i] = tempJumpPoint;
        }

        try
        {
            return new JumpMemory(jumpPoints);
        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
    //endregion
}
