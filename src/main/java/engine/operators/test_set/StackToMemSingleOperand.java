package engine.operators.test_set;

import com.ampex.amperabase.ITransAPI;
import engine.binary.IBinary;
import engine.data.*;
import engine.operators.IOperator;
import engine.program.IOPCode;
import engine.program.IProgram;

import java.util.Stack;

/**
 * Created by Queue on 3/8/2018.
 */
public class StackToMemSingleOperand implements IOperator
{
    //region Constant Values
    public static final int BASE_CODE = 51;

    public static final int MAX_EXECUTIONS = -1;

    public static final String KEYWORD = "STMSO";
    //endregion

    public String getKeyword()
    {
        return KEYWORD;
    }

    public int getMaxExecutions()
    {
        return MAX_EXECUTIONS;
    }

    public int getBaseCode()
    {
        return BASE_CODE;
    }

    public void execute(Stack<IDataElement> _stack, IBinary _binary, IProgram _program, IConstantMemory _constantMemory, IJumpMemory _jumpMemory, IWritableMemory _writableMemory, IOPCode _opCode, ITransAPI _transaction, byte[] _executionAddress) throws Exception
    {
        IDataElement element = _stack.pop();//_memory.getElement(_opCode.getVariantCode());

        if(element == null)
        {
            throw new Exception();
        }

        _writableMemory.setElement(element,_opCode.getVariantCode());
    }
}
