package engine.operators.test_set;

import com.ampex.amperabase.ITransAPI;
import engine.binary.IBinary;
import engine.data.*;
import engine.operators.IOperator;
import engine.program.IOPCode;
import engine.program.IProgram;

import java.util.Stack;

/**
 * Created by Queue on 3/7/2018.
 */
public class MemToStackNoOperand implements IOperator
{
    //region Constant Values
    public static final int BASE_CODE = 77;

    public static final int MAX_EXECUTIONS = -1;

    public static final String KEYWORD = "MTSNO";
    //endregion

    public String getKeyword()
    {
        return KEYWORD;
    }

    public int getMaxExecutions()
    {
        return MAX_EXECUTIONS;
    }

    public int getBaseCode()
    {
        return BASE_CODE;
    }

    public void execute(Stack<IDataElement> _stack, IBinary _binary, IProgram _program, IConstantMemory _constantMemory, IJumpMemory _jumpMemory, IWritableMemory _writableMemory, IOPCode _opCode, ITransAPI _transaction, byte[] _executionAddress) throws Exception
    {
        IDataElement element = _writableMemory.getElement(_opCode.getVariantCode());

        if(element == null)
        {
            throw new Exception();
        }

        _stack.push(element);
    }
}
