package engine.operators.test_set;

import com.ampex.amperabase.ITransAPI;
import engine.binary.IBinary;
import engine.data.*;
import engine.data.DataElement;
import engine.operators.IOperator;
import engine.program.IOPCode;
import engine.program.IProgram;

import java.math.BigInteger;
import java.util.Stack;

/**
 * Created by Queue on 3/7/2018.
 */
public class BIDivision implements IOperator
{
    //region Constant Values
    public static final int BASE_CODE = 56;

    public static final int MAX_EXECUTIONS = -1;

    public static final String KEYWORD = "BID";
    //endregion

    public String getKeyword()
    {
        return KEYWORD;
    }

    public int getMaxExecutions()
    {
        return MAX_EXECUTIONS;
    }

    public int getBaseCode()
    {
        return BASE_CODE;
    }

    public void execute(Stack<IDataElement> _stack, IBinary _binary, IProgram _program, IConstantMemory _constantMemory, IJumpMemory _jumpMemory, IWritableMemory _writableMemory, IOPCode _opCode, ITransAPI _transaction, byte[] _executionAddress) throws Exception
    {
        BigInteger operand1 = _stack.pop().getDataAsBigInteger();
        BigInteger operand2 = _stack.pop().getDataAsBigInteger();

        BigInteger operator = operand1.divide(operand2);

        IDataElement result = new DataElement(operator.toByteArray());

        _stack.push(result);
    }
}
