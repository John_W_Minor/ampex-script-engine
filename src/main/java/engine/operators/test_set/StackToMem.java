package engine.operators.test_set;

import com.ampex.amperabase.ITransAPI;
import engine.binary.IBinary;
import engine.data.*;
import engine.operators.IOperator;
import engine.program.IOPCode;
import engine.program.IProgram;

import java.math.BigInteger;
import java.util.Stack;

/**
 * Created by Queue on 3/7/2018.
 */
public class StackToMem implements IOperator
{
    //region Constant Values
    public static final int BASE_CODE = 53;

    public static final int MAX_EXECUTIONS = -1;

    public static final String KEYWORD = "STM";
    //endregion

    public String getKeyword()
    {
        return KEYWORD;
    }

    public int getMaxExecutions()
    {
        return MAX_EXECUTIONS;
    }

    public int getBaseCode()
    {
        return BASE_CODE;
    }

    public void execute(Stack<IDataElement> _stack, IBinary _binary, IProgram _program, IConstantMemory _constantMemory, IJumpMemory _jumpMemory, IWritableMemory _writableMemory, IOPCode _opCode, ITransAPI _transaction, byte[] _executionAddress) throws Exception
    {
        BigInteger biIndex = _stack.pop().getDataAsBigInteger();

        if(biIndex == null)
        {
            throw new Exception();
        }

        int index = biIndex.intValue();

        IDataElement element = _stack.pop();

        if(!_writableMemory.setElement(element, index))
        {
            throw new Exception();
        }
    }
}
