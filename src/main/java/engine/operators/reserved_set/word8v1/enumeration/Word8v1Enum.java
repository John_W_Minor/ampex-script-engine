package engine.operators.reserved_set.word8v1.enumeration;

import engine.operators.reserved_set.word8v1.PI.*;

public enum Word8v1Enum
{
    NO_OPERATION(0, "noop"),
    TERMINATE(1, "term"),
    PUT_INTEGER_0(2 + PI0Word8.VAL, "PI" + PI0Word8.VAL),
    PUT_INTEGER_1(2 + PI1Word8.VAL, "PI" + PI1Word8.VAL),
    PUT_INTEGER_2(2 + PI2Word8.VAL, "PI" + PI2Word8.VAL),
    PUT_INTEGER_3(2 + PI3Word8.VAL, "PI" + PI3Word8.VAL),
    PUT_INTEGER_4(2 + PI4Word8.VAL, "PI" + PI4Word8.VAL),
    PUT_INTEGER_5(2 + PI5Word8.VAL, "PI" + PI5Word8.VAL),
    PUT_INTEGER_6(2 + PI6Word8.VAL, "PI" + PI6Word8.VAL),
    PUT_INTEGER_7(2 + PI7Word8.VAL, "PI" + PI7Word8.VAL),
    PUT_INTEGER_8(2 + PI8Word8.VAL, "PI" + PI8Word8.VAL),
    PUT_INTEGER_9(2 + PI9Word8.VAL, "PI" + PI9Word8.VAL),
    PUT_INTEGER_10(2 + PI10Word8.VAL, "PI" + PI10Word8.VAL),
    PUT_INTEGER_11(2 + PI11Word8.VAL, "PI" + PI11Word8.VAL),
    PUT_INTEGER_12(2 + PI12Word8.VAL, "PI" + PI12Word8.VAL),
    PUT_INTEGER_13(2 + PI13Word8.VAL, "PI" + PI13Word8.VAL),
    PUT_INTEGER_14(2 + PI14Word8.VAL, "PI" + PI14Word8.VAL),
    PUT_INTEGER_15(2 + PI15Word8.VAL, "PI" + PI15Word8.VAL),
    PUT_INTEGER_16(2 + PI16Word8.VAL, "PI" + PI16Word8.VAL),
    PUT_INTEGER_17(2 + PI17Word8.VAL, "PI" + PI17Word8.VAL),
    PUT_INTEGER_18(2 + PI18Word8.VAL, "PI" + PI18Word8.VAL),
    PUT_INTEGER_19(2 + PI19Word8.VAL, "PI" + PI19Word8.VAL),
    PUT_INTEGER_20(2 + PI20Word8.VAL, "PI" + PI20Word8.VAL),
    PUT_INTEGER_21(2 + PI21Word8.VAL, "PI" + PI21Word8.VAL),
    PUT_INTEGER_22(2 + PI22Word8.VAL, "PI" + PI22Word8.VAL),
    PUT_INTEGER_23(2 + PI23Word8.VAL, "PI" + PI23Word8.VAL),
    PUT_INTEGER_24(2 + PI24Word8.VAL, "PI" + PI24Word8.VAL),
    PUT_INTEGER_25(2 + PI25Word8.VAL, "PI" + PI25Word8.VAL),
    PUT_INTEGER_26(2 + PI26Word8.VAL, "PI" + PI26Word8.VAL),
    PUT_INTEGER_27(2 + PI27Word8.VAL, "PI" + PI27Word8.VAL),
    PUT_INTEGER_28(2 + PI28Word8.VAL, "PI" + PI28Word8.VAL),
    PUT_INTEGER_29(2 + PI29Word8.VAL, "PI" + PI29Word8.VAL),
    PUT_INTEGER_30(2 + PI30Word8.VAL, "PI" + PI30Word8.VAL),
    PUT_INTEGER_31(2 + PI31Word8.VAL, "PI" + PI31Word8.VAL),
    DUPLICATE(34, "dup"),
    CLEAR_STACK(35, "csk"),
    POP_POP_PUSH(36, "ppp"),
    LOAD_CONSTANT_TO_STACK(37, "lcsk"),
    LOAD_PC_CONSTANT_TO_STACK(38, "lpccsk"),
    LOAD_MEMORY_TO_STACK(39, "lmsk"),
    LOAD_PC_MEMORY_TO_STACK(40, "lpcmsk"),
    SAVE_STACK_TO_MEMORY(41, "sav"),
    SAVE_STACK_TO_PC_MEMORY(42, "savpc"),
    INT_EQUALS(43, "ieq"),
    INT_NOT_EQUALS(44, "ineq"),
    INT_GREATER_THAN(45, "gtn"),
    INT_LESS_THAN(46, "ltn"),
    STRING_EQUALS(47, "seq"),
    STRING_NOT_EQUALS(48, "sneq"),
    BRANCH(49, "brn"),
    LOAD_JUMP_TO_STACK(50, "ljsk");


    private final int baseCode;
    private final String keyword;

    Word8v1Enum(int _baseCode, String _keyword)
    {
        baseCode=_baseCode;
        keyword=_keyword;
    }

    public int getBaseCode()
    {
        return baseCode;
    }

    public String getKeyword()
    {
        return keyword;
    }
}
