package engine.operators.reserved_set.word8v1.PI;

import amp.ByteTools;
import com.ampex.amperabase.ITransAPI;
import engine.binary.IBinary;
import engine.data.*;
import engine.operators.reserved_set.word8v1.IOperatorReservedWord8;
import engine.program.IOPCode;
import engine.program.IProgram;

import java.util.Stack;

public class PI11Word8 implements IOperatorReservedWord8
{
    //region Constant Values
    public static final int VAL = 11;

    public static final String VALSTR = String.valueOf(VAL);

    public static final int BASE_CODE = 2 + VAL;

    public static final int MAX_EXECUTIONS = -1;

    public static final String KEYWORD = "pi"+VALSTR;
    //endregion

    public String getKeyword()
    {
        return KEYWORD;
    }

    public int getMaxExecutions()
    {
        return MAX_EXECUTIONS;
    }

    public int getBaseCode()
    {
        return BASE_CODE;
    }

    public void execute(Stack<IDataElement> _stack, IBinary _binary, IProgram _program, IConstantMemory _constantMemory, IJumpMemory _jumpMemory, IWritableMemory _writableMemory, IOPCode _opCode, ITransAPI _transaction, byte[] _executionAddress) throws Exception
    {
        IDataElement integerElement = new DataElement(ByteTools.deconstructInt(VAL));

        _stack.push(integerElement);
    }
}
