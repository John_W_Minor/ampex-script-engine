package engine.operators.reserved_set.word8v1;

import com.ampex.amperabase.ITransAPI;
import engine.binary.IBinary;
import engine.data.*;
import engine.operators.reserved_set.word8v1.enumeration.Word8v1Enum;
import engine.program.IOPCode;
import engine.program.IProgram;

import java.util.Stack;

public class PopPopPushWord8 implements IOperatorReservedWord8
{
    //region Constant Values
    public static final int BASE_CODE = Word8v1Enum.POP_POP_PUSH.getBaseCode();

    public static final int MAX_EXECUTIONS = -1;

    public static final String KEYWORD = Word8v1Enum.POP_POP_PUSH.getKeyword();
    //endregion

    public String getKeyword()
    {
        return KEYWORD;
    }

    public int getMaxExecutions()
    {
        return MAX_EXECUTIONS;
    }

    public int getBaseCode()
    {
        return BASE_CODE;
    }

    public void execute(Stack<IDataElement> _stack, IBinary _binary, IProgram _program, IConstantMemory _constantMemory, IJumpMemory _jumpMemory, IWritableMemory _writableMemory, IOPCode _opCode, ITransAPI _transaction, byte[] _executionAddress) throws Exception
    {
        IDataElement tempElement = _stack.pop();
        _stack.pop();
        _stack.push(tempElement);
    }
}
