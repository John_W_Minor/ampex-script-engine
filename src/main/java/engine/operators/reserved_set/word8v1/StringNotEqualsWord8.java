package engine.operators.reserved_set.word8v1;

import amp.ByteTools;
import com.ampex.amperabase.ITransAPI;
import engine.binary.IBinary;
import engine.data.*;
import engine.exceptions.ASEException;
import engine.operators.reserved_set.word8v1.enumeration.Word8v1Enum;
import engine.program.IOPCode;
import engine.program.IProgram;

import java.util.Stack;

public class StringNotEqualsWord8 implements IOperatorReservedWord8
{
    //region Constant Values
    public static final int BASE_CODE = Word8v1Enum.STRING_NOT_EQUALS.getBaseCode();

    public static final int MAX_EXECUTIONS = -1;

    public static final String KEYWORD = Word8v1Enum.STRING_NOT_EQUALS.getKeyword();
    //endregion

    public String getKeyword()
    {
        return KEYWORD;
    }

    public int getMaxExecutions()
    {
        return MAX_EXECUTIONS;
    }

    public int getBaseCode()
    {
        return BASE_CODE;
    }

    public void execute(Stack<IDataElement> _stack, IBinary _binary, IProgram _program, IConstantMemory _constantMemory, IJumpMemory _jumpMemory, IWritableMemory _writableMemory, IOPCode _opCode, ITransAPI _transaction, byte[] _executionAddress) throws Exception
    {
        String a = _stack.pop().getDataAsString();
        String b = _stack.pop().getDataAsString();

        if(a == null || b == null)
        {
            throw new ASEException("Unable to convert DataElement to String.");
        }

        int returnValue = a.equals(b) ? 0 : 1;

        IDataElement returnDataElement = new DataElement(ByteTools.deconstructInt(returnValue));

        _stack.push(returnDataElement);
    }
}
