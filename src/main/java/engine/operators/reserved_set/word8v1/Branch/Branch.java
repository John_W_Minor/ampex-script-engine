package engine.operators.reserved_set.word8v1.Branch;

import com.ampex.amperabase.ITransAPI;
import engine.ASEConstants;
import engine.binary.IBinary;
import engine.data.*;
import engine.exceptions.ASEException;
import engine.operators.reserved_set.word8v1.IOperatorReservedWord8;
import engine.operators.reserved_set.word8v1.enumeration.Word8v1Enum;
import engine.program.IOPCode;
import engine.program.IProgram;

import java.util.Stack;

public class Branch implements IOperatorReservedWord8
{
    //region Constant Values
    public static final int BASE_CODE = Word8v1Enum.BRANCH.getBaseCode();

    public static final int MAX_EXECUTIONS = -1;

    public static final String KEYWORD = Word8v1Enum.BRANCH.getKeyword();
    //endregion

    public String getKeyword()
    {
        return KEYWORD;
    }

    public int getMaxExecutions()
    {
        return MAX_EXECUTIONS;
    }

    public int getBaseCode()
    {
        return BASE_CODE;
    }

    public void execute(Stack<IDataElement> _stack, IBinary _binary, IProgram _program, IConstantMemory _constantMemory, IJumpMemory _jumpMemory, IWritableMemory _writableMemory, IOPCode _opCode, ITransAPI _transaction, byte[] _executionAddress) throws Exception
    {
        Integer jumpPosition = _stack.pop().getDataAsInt();

        Integer switchData = _stack.pop().getDataAsInt();

        if (jumpPosition == null || switchData == null)
        {
            throw new ASEException("Unable to read branch data.");
        }

        if (switchData == 0)
        {
            if (jumpPosition > _program.getProgramCounter())
            {
                _program.setProgramCounter(jumpPosition);
                return;
            }

            throw new ASEException("Cannot jump backwards.");
        }
    }
}
