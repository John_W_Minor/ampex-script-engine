package engine.operators.reserved_set.word8v1;

import com.ampex.amperabase.ITransAPI;
import engine.ASEConstants;
import engine.binary.IBinary;
import engine.data.*;
import engine.exceptions.ASEException;
import engine.operators.reserved_set.word8v1.enumeration.Word8v1Enum;
import engine.program.IOPCode;
import engine.program.IProgram;

import java.util.Stack;

public class LoadPCConstantToStackWord8 implements IOperatorReservedWord8
{
    //region Constant Values
    public static final int BASE_CODE = Word8v1Enum.LOAD_PC_CONSTANT_TO_STACK.getBaseCode();

    public static final int MAX_EXECUTIONS = -1;

    public static final String KEYWORD = Word8v1Enum.LOAD_PC_CONSTANT_TO_STACK.getKeyword();
    //endregion

    public String getKeyword()
    {
        return KEYWORD;
    }

    public int getMaxExecutions()
    {
        return MAX_EXECUTIONS;
    }

    public int getBaseCode()
    {
        return BASE_CODE;
    }

    public void execute(Stack<IDataElement> _stack, IBinary _binary, IProgram _program, IConstantMemory _constantMemory, IJumpMemory _jumpMemory, IWritableMemory _writableMemory, IOPCode _opCode, ITransAPI _transaction, byte[] _executionAddress) throws Exception
    {
        int constAddress = _program.getProgramCounter();

        if(constAddress < 0 || constAddress > (ASEConstants.CONSTANT_MEMORY_MAX_DATA_ELEMENTS - 1))
        {
            throw new ASEException("Program counter is too large to be used as a memory address.");
        }

        IDataElement constData = _constantMemory.getElement(constAddress);

        //if(constData == null)
        {
            //throw new ASEException("Null data loaded onto stack from constant memory.");
        }

        _stack.push(constData);
    }
}