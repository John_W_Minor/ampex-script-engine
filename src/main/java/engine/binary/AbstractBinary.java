package engine.binary;

import amp.ByteTools;
import com.ampex.amperabase.KeyType;
import engine.ASEConstants;
import engine.data.IConstantMemory;
import engine.data.IJumpMemory;
import engine.exceptions.ASEException;
import engine.program.IProgram;

import java.nio.ByteBuffer;

public abstract class AbstractBinary implements IBinary
{
    //region Member Fields
    protected final IProgram program;
    protected final IConstantMemory constantMemory;
    protected final IJumpMemory jumpMemory;
    protected final boolean word8;
    protected final int BCEVersion;
    protected final byte[] entropy;
    protected final Long timestamp;
    protected final byte[] publicKey;
    protected final KeyType publicKeyType;
    protected final byte[] feeAddress;
    protected final long feeMultiplier;
    //endregion

    //region Constructors
    public AbstractBinary(IProgram _program,
                          IConstantMemory _constantMemory,
                          IJumpMemory _jumpMemory,
                          boolean _word8,
                          int _BCEVersion,
                          byte[] _entropy,
                          long _timestamp,
                          byte[] _publicKey,
                          KeyType _publicKeyType,
                          byte[] _feeAddress,
                          long _feeMultiplier) throws Exception
    {

        //region Argument Sentinels
        if (_program == null || !_program.isSealed())
        {
            throw new ASEException("Bad _program argument.");
        }

        if (_constantMemory == null)
        {
            throw new ASEException("Null _constantMemory argument.");
        }

        if (_jumpMemory == null)
        {
            throw new ASEException("Null _jumpMemory argument.");
        }

        if (_BCEVersion < 1)
        {
            throw new ASEException("Bad BCEVersion: " + _BCEVersion + ".");
        }

        if (_entropy == null || _entropy.length == 0 || _entropy.length > ASEConstants.ENTROPY_MAX_MAX_LENGTH)
        {
            throw new ASEException("Bad _entropy argument.");
        }

        if (_publicKey == null || _publicKey.length == 0 || _publicKey.length > ASEConstants.PUBLIC_KEY_MAX_LENGTH)
        {
            throw new ASEException("Bad _publicKey argument.");
        }

        if (_publicKeyType == null)
        {
            _publicKeyType = KeyType.NONE;
        }

        if (_feeAddress != null)
        {
            if (_feeAddress.length == 0 || _feeAddress.length > ASEConstants.FEE_ADDRESS_MAX_SIZE)
            {
                throw new ASEException("Bad _feeAddressArgument");
            }
        }

        if (_feeMultiplier < 0)
        {
            throw new ASEException("Bad fee multiplier: " + _feeMultiplier + ".");
        }
        //endregion

        program = _program;
        program.seal();
        constantMemory = _constantMemory;
        jumpMemory = _jumpMemory;
        word8 = _word8;

        BCEVersion = _BCEVersion;

        entropy = _entropy.clone();
        timestamp = _timestamp;
        publicKey = _publicKey.clone();
        publicKeyType = _publicKeyType;

        if (_feeAddress != null)
        {
            feeAddress = _feeAddress.clone();
        } else
        {
            feeAddress = null;
        }

        feeMultiplier = _feeMultiplier;
    }
    //endregion

    //region Getters
    @Override
    public IProgram getProgram()
    {
        return program;
    }

    @Override
    public IConstantMemory getConstantMemory()
    {
        return constantMemory;
    }

    @Override
    public IJumpMemory getJumpMemory()
    {
        return jumpMemory;
    }

    @Override
    public boolean isWord8()
    {
        return word8;
    }

    @Override
    public int getBCEVersion()
    {
        return BCEVersion;
    }

    @Override
    public byte[] getEntropy()
    {
        return entropy.clone();
    }

    @Override
    public long getTimestamp()
    {
        return timestamp;
    }

    @Override
    public byte[] getPublicKey()
    {
        return publicKey.clone();
    }

    @Override
    public KeyType getPublicKeyType()
    {
        return publicKeyType;
    }

    @Override
    public byte[] getFeeAddress()
    {
        if (feeAddress != null)
        {
            return feeAddress.clone();
        }
        return null;
    }

    @Override
    public long getFeeMultiplier()
    {
        return feeMultiplier;
    }

    @Override
    public byte[] getProgramIdentityBytes()
    {
        byte[] word8Bytes = ByteTools.deconstructBoolean(word8);
        byte[] versionBytes = ByteTools.deconstructInt(BCEVersion);
        byte[] programBytes = program.serializeToBytes();
        byte[] jumpBytes = jumpMemory.getIdentityBytes();

        byte[] feeMultBytes = ByteTools.deconstructLong(feeMultiplier);

        int feeAddressLength = 0;

        if (feeAddress != null)
        {
            feeAddressLength = feeAddress.length;
        }

        ByteBuffer buff = ByteBuffer.allocate(word8Bytes.length + versionBytes.length + programBytes.length + jumpBytes.length + feeMultBytes.length + feeAddressLength)
                .put(word8Bytes).put(versionBytes).put(programBytes).put(jumpBytes).put(feeMultBytes);

        if (feeAddress != null)
        {
            buff.put(feeAddress);
        }

        return buff.array();
    }
    //endregion
}
