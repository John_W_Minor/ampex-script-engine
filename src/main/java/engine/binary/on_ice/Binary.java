package engine.binary.on_ice;

import amp.*;
import amp.classification.AmpClassCollection;
import amp.classification.classes.AC_SingleElement;
import amp.group_ids.GroupID;
import amp.group_primitives.UnpackedGroup;
import amp.headless_amplet.HeadlessAmpletWritable;
import amp.headless_amplet.IHeadlessAmpletTwinReadable;
import amp.headless_amplet.IHeadlessAmpletWritable;
import com.ampex.amperabase.KeyType;
import engine.ASEConstants;
import engine.ASELogging;
import engine.binary.AbstractBinary;
import engine.data.IConstantMemory;
import engine.data.IJumpMemory;
import engine.data.constant_memory.ConstantMemoryFactory;
import engine.data.jump_memory.JumpMemoryFactory;
import engine.program.IProgram;
import engine.program.Program;
import logging.IAmpexLogger;

public class Binary extends AbstractBinary
{

    //region Constructors
    public Binary(IProgram _program,
                  IConstantMemory _constantMemory,
                  IJumpMemory _jumpMemory,
                  boolean _word8,
                  int _BCEVersion,
                  byte[] _entropy,
                  long _timestamp,
                  byte[] _publicKey,
                  KeyType _publicKeyType,
                  byte[] _feeAddress,
                  long _feeMultiplier) throws Exception
    {
        super(_program, _constantMemory, _jumpMemory, _word8, _BCEVersion, _entropy, _timestamp, _publicKey, _publicKeyType, _feeAddress, _feeMultiplier);

    }
    //endregion

    //region Serialization

    //region Class and Group IDs
    public static final GroupID CONSTANT_MEMORY_GI = new GroupID(9953342, 345524, "Constant Memory hpamplet.");
    public static final GroupID HAMPLET_KEY_GI = new GroupID(953221, 5543, "Word8, version, timestamp hamplet.");
    public static final GroupID ENTROPY_GI = new GroupID(43565, 12311, "Entropy byte array.");
    public static final GroupID PUBLIC_KEY_GI = new GroupID(1118221, 3455, "Public key byte array.");
    public static final GroupID PUBLIC_KEY_TYPE_GI = new GroupID(1345, 555213, "Public key type enum");
    public static final GroupID FEE_ADDRESS_GI = new GroupID(5342, 66666, "Fee address byte array.");
    //endregion

    @Override
    public Amplet serializeToAmplet()
    {
        //region Constant Memory
        byte[] constantMemoryBytes = constantMemory.serializeToBytes();

        AC_SingleElement constantMemoryAC = AC_SingleElement.create(CONSTANT_MEMORY_GI, constantMemoryBytes);
        //endregion

        //region Jump Memory, Word8, Version, Timestamp Hamplet
        IHeadlessAmpletWritable hamplet = HeadlessAmpletWritable.create();

        try
        {
            for (int i = 0; i < ASEConstants.JUMP_MEMORY_MAX_ELEMENTS; i++)
            {
                hamplet.addElement(jumpMemory.getJumpPoint(i));
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }

        hamplet.addElement(word8);
        hamplet.addElement(BCEVersion);
        hamplet.addElement(timestamp);
        hamplet.addElement(feeMultiplier);

        AC_SingleElement hampletAC = AC_SingleElement.create(HAMPLET_KEY_GI, hamplet);
        //endregion

        //region Entropy and Public Key and Fee Address
        AC_SingleElement entropyAC = AC_SingleElement.create(ENTROPY_GI, entropy);
        AC_SingleElement publicKeyAC = AC_SingleElement.create(PUBLIC_KEY_GI, publicKey);
        AC_SingleElement feeAddressAC = null;
        if (feeAddress != null)
        {
            feeAddressAC = AC_SingleElement.create(FEE_ADDRESS_GI, feeAddress);
        }
        //endregion

        //region Binary Class Collection
        AmpClassCollection binaryCC = new AmpClassCollection();

        binaryCC.addClass(constantMemoryAC);
        binaryCC.addClass(hampletAC);
        binaryCC.addClass(entropyAC);
        binaryCC.addClass(publicKeyAC);

        if (publicKeyType != null)
        {
            AC_SingleElement publicKeyTypeAC = AC_SingleElement.create(PUBLIC_KEY_TYPE_GI, publicKeyType.getValue());
            binaryCC.addClass(publicKeyTypeAC);
        }
        if (feeAddressAC != null)
        {
            binaryCC.addClass(feeAddressAC);
        }
        //endregion

        //region Amplet and Program
        Amplet binaryAmplet = binaryCC.serializeToAmplet();

        binaryAmplet.setTrailer(program);
        //endregion

        return binaryAmplet;
    }

    public static Binary deserializeFromAmplet(Amplet _binaryAmplet)
    {
        IAmpexLogger logger = ASELogging.getLogger();

        //region Argument Sentinel
        if (_binaryAmplet == null)
        {
            if (logger != null)
            {
                logger.warn("\nAmplet null when deserializing Binary.");
            }
            return null;
        }
        //endregion

        //region Constant Memory
        UnpackedGroup constantMemoryUG = _binaryAmplet.unpackGroup(CONSTANT_MEMORY_GI);

        if (constantMemoryUG == null)
        {
            if (logger != null)
            {
                logger.warn("\nConstant Memory Unpacked Group null when deserializing Binary.");
            }
            return null;
        }

        byte[] constantMemoryBytes = constantMemoryUG.getElement(0);

        if (constantMemoryBytes == null)
        {
            if (logger != null)
            {
                logger.warn("\nConstant Memory bytes null when deserializing Binary.");
            }
            return null;
        }

        IConstantMemory constantMemory = ConstantMemoryFactory.build(constantMemoryBytes);

        if (constantMemory == null)
        {
            if (logger != null)
            {
                logger.warn("\nConstant Memory null when deserializing Binary.");
            }
            return null;
        }

        //endregion

        //region Entropy
        UnpackedGroup entropyUG = _binaryAmplet.unpackGroup(ENTROPY_GI);

        if (entropyUG == null)
        {
            if (logger != null)
            {
                logger.warn("\nEntropy Unpacked Group null when deserializing Binary.");
            }
            return null;
        }

        byte[] entropy = entropyUG.getElement(0);

        if (entropy == null)
        {
            if (logger != null)
            {
                logger.warn("\nEntropy null when deserializing Binary.");
            }
            return null;
        }
        //endregion

        //region Public Key
        UnpackedGroup publicKeyUG = _binaryAmplet.unpackGroup(PUBLIC_KEY_GI);

        if (publicKeyUG == null)
        {
            if (logger != null)
            {
                logger.warn("\nPublic Key Unpacked Group null when deserializing Binary.");
            }
            return null;
        }

        byte[] publicKey = publicKeyUG.getElement(0);

        if (publicKey == null)
        {
            if (logger != null)
            {
                logger.warn("\nPublic Key null when deserializing Binary.");
            }
            return null;
        }
        //endregion

        //region Public Key Type
        UnpackedGroup publicKeyTypeUG = _binaryAmplet.unpackGroup(PUBLIC_KEY_TYPE_GI);

        KeyType publicKeyType = KeyType.NONE;

        if (publicKeyTypeUG != null)
        {
            byte[] publicKeyTypeArray = publicKeyTypeUG.getElement(0);

            if (publicKeyTypeArray != null && publicKeyTypeArray.length == 1)
            {
                byte publicKeyTypeValue = publicKeyTypeArray[0];

                if (publicKeyTypeValue == KeyType.BRAINPOOLP512T1.getValue())
                {
                    publicKeyType = KeyType.BRAINPOOLP512T1;
                }

                if (publicKeyTypeValue == KeyType.ED25519.getValue())
                {
                    publicKeyType = KeyType.ED25519;
                }
            }
        }

        //endregion

        //region Fee Address
        UnpackedGroup feeAddressUG = _binaryAmplet.unpackGroup(FEE_ADDRESS_GI);

        byte[] feeAddress = null;

        if (feeAddressUG != null)
        {
            feeAddress = feeAddressUG.getElement(0);
        }
        //endregion

        //region Hamplet
        UnpackedGroup hampletUG = _binaryAmplet.unpackGroup(HAMPLET_KEY_GI);

        if (hampletUG == null)
        {
            if (logger != null)
            {
                logger.warn("\nHamplet Unpacked Group null when deserializing Binary.");
            }
            return null;
        }

        IHeadlessAmpletTwinReadable hamplet = hampletUG.getElementAsIHeadlessAmpletTwinReadable(0);

        if (hamplet == null)
        {
            if (logger != null)
            {
                logger.warn("\nHamplet null when deserializing Binary.");
            }
            return null;
        }
        //endregion

        //region Jump Memory, Word8, Version, Timestamp

        short[] jumpPoints = new short[ASEConstants.JUMP_MEMORY_MAX_ELEMENTS];

        for (int i = 0; i < ASEConstants.JUMP_MEMORY_MAX_ELEMENTS; i++)
        {
            Short tempJumpPoint = hamplet.getNextShort();

            if (tempJumpPoint == null)
            {
                if (logger != null)
                {
                    logger.warn("\nJump Point null when deserializing Binary.");
                }
                return null;
            }

            jumpPoints[i] = tempJumpPoint;
        }

        IJumpMemory jumpMemory = null;

        try
        {
            jumpMemory = JumpMemoryFactory.build(jumpPoints);
        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }

        Boolean word8 = hamplet.getNextBoolean();

        Integer BCEVersion = hamplet.getNextInt();

        Long timestamp = hamplet.getNextLong();

        Long feeMult = hamplet.getNextLong();

        if (jumpMemory == null || word8 == null || BCEVersion == null || timestamp == null || feeMult == null)
        {
            if (logger != null)
            {
                logger.warn("\nJump Memory, Word8, Version, or Timestamp null when deserializing Binary.");
            }
            return null;
        }
        //endregion

        //region Program
        byte[] bytes = _binaryAmplet.getTrailer();

        IProgram program = null;

        try
        {
            program = new Program(bytes);
        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
        //endregion

        try
        {
            return new Binary(program, constantMemory, jumpMemory, word8, BCEVersion, entropy, timestamp, publicKey, publicKeyType, feeAddress, feeMult);
        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public byte[] serializeToBytes()
    {
        return serializeToAmplet().serializeToBytes();
    }
    //endregion
}
