package engine.binary;

import amp.Amplet;
import com.ampex.amperabase.KeyType;
import engine.binary.on_ice.Binary;
import engine.data.IConstantMemory;
import engine.data.IJumpMemory;
import engine.program.IProgram;

public class BinaryFactory
{

    public static IBinary build(IProgram _program,
                                IConstantMemory _constantMemory,
                                IJumpMemory _jumpMemory,
                                boolean _word8,
                                int _BCEVersion,
                                byte[] _entropy,
                                long _timestamp,
                                byte[] _publicKey,
                                KeyType _publicKeyType,
                                byte[] _feeAddress,
                                long _feeMultiplier) throws Exception
    {
        return new Binary(_program, _constantMemory, _jumpMemory, _word8, _BCEVersion, _entropy, _timestamp, _publicKey, _publicKeyType, _feeAddress, _feeMultiplier);
    }

    public static IBinary buildFromAmplet(Amplet _binaryAmplet)
    {
        try
        {
            return Binary.deserializeFromAmplet(_binaryAmplet);
        } catch (Exception e)
        {
            return null;
        }
    }
}
