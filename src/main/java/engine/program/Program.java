package engine.program;

import amp.headless_amplet.HeadlessAmpletTwinReadable;
import amp.headless_amplet.HeadlessAmpletWritable;
import amp.headless_amplet.IHeadlessAmpletTwinReadable;
import amp.headless_amplet.IHeadlessAmpletWritable;
import engine.ASEConstants;
import engine.exceptions.ASEException;

public class Program implements IProgram
{
    //region Member Fields
    private IHeadlessAmpletTwinReadable programHamplet = null;

    private boolean sealed = false;

    private boolean word8 = true;

    private boolean counterNoOffset = true;
    //endregion

    //region Constructors
    public Program()
    {
        programHamplet = HeadlessAmpletTwinReadable.create(new byte[0]);
    }

    public Program(byte[] _programBytes) throws Exception
    {
        this(HeadlessAmpletTwinReadable.create(_programBytes));
    }

    private Program(HeadlessAmpletTwinReadable _programHamplet) throws Exception
    {
        if (_programHamplet == null || _programHamplet.getByteFootprint() > ASEConstants.PROGRAM_MAX_SIZE)
        {
            throw new ASEException("Bad program.");
        }

        programHamplet = _programHamplet;
        seal();
        setProgramCounter(0);
    }
    //endregion

    //region Program Counter
    @Override
    public void setProgramCounter(int _index) throws Exception
    {
        if (!sealed)
        {
            return;
        }

        if (!word8)
        {
            _index *= 2;
        }

        if (_index < 0)
        {
            throw new ASEException("Invalid program counter position: " + _index);
        }

        if (_index >= programHamplet.getMaxCursorPosition())
        {
            throw new ASEException("Invalid program counter position: " + _index);
        }

        programHamplet.setCursor(_index);

        counterNoOffset = true;
    }

    @Override
    public int getProgramCounter()
    {
        int counter = programHamplet.getCursor();

        if (!word8)
        {
            counter /= 2;
        }

        if(!counterNoOffset)
        {
            counter--;
        }

        return counter;
    }

    @Override
    public void decrementProgramCounter()
    {
        int counter = programHamplet.getCursor();
        if (word8)
        {
            programHamplet.setCursor(counter - 1);
        } else
        {
            programHamplet.setCursor(counter - 2);
        }

        counterNoOffset = true;
    }

    @Override
    public void incrementProgramCounter()
    {
        int counter = programHamplet.getCursor();
        if (word8)
        {
            programHamplet.setCursor(counter + 1);
        } else
        {
            programHamplet.setCursor(counter + 2);
        }

        counterNoOffset = true;
    }
    //endregion

    //region Opcodes
    @Override
    public boolean addOPCode(IOPCode _opCode)
    {
        return addOPCode(_opCode, true);
    }

    @Override
    public boolean addOPCode(IOPCode _opCode, boolean _word8)
    {
        if (!sealed && programHamplet.getByteFootprint() < (ASEConstants.PROGRAM_MAX_SIZE - 8))
        {
            int cursor = programHamplet.getCursor();
            IHeadlessAmpletWritable tempWrite = HeadlessAmpletWritable.create(programHamplet.serializeToBytes());

            if (_word8)
            {
                tempWrite.addElement((byte) _opCode.getBaseCode());
            } else
            {
                tempWrite.addElement(_opCode.convertToChar());
            }

            programHamplet = HeadlessAmpletTwinReadable.create(tempWrite.serializeToBytes());
            programHamplet.setCursor(cursor);
            return true;
        }
        return false;
    }

    @Override
    public IOPCode getNextWord() throws Exception
    {
        if (word8)
        {
            return getNextWord8();
        }
        return getNextWord16();
    }

    private IOPCode getNextWord8() throws Exception
    {
        if (!sealed)
        {
            return null;
        }
        Byte a = programHamplet.getNextByte();
        counterNoOffset = false;

        if (a == null)
        {
            return null;
        }

        return new OPCode(a);
    }

    private IOPCode getNextWord16() throws Exception
    {
        if (!sealed)
        {
            return null;
        }
        Byte a = programHamplet.getNextByte();
        Byte b = programHamplet.getNextByte();
        counterNoOffset = false;

        if (a == null || b == null)
        {
            return null;
        }

        return new OPCode(a, b);
    }
    //endregion

    //region Word Mode
    @Override
    public void word8Mode()
    {
        word8 = true;
    }

    @Override
    public void word16Mode()
    {
        word8 = false;
    }

    @Override
    public boolean isWord8()
    {
        return word8;
    }
    //endregion

    //region Sealing
    @Override
    public void seal()
    {
        sealed = true;
    }

    @Override
    public boolean isSealed()
    {
        return sealed;
    }
    //endregion

    @Override
    public IProgram copyProgram()
    {
        seal();
        try
        {
            return new Program(HeadlessAmpletTwinReadable.createTwin(programHamplet));
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    //region Serialization
    @Override
    public byte[] serializeToBytes()
    {
        return programHamplet.serializeToBytes();
    }
    //endregion
}
