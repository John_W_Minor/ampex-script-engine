package engine.program;

import amp.ByteTools;
import engine.ASEConstants;
import engine.exceptions.ASEException;

public class OPCode implements IOPCode
{
    private int variantCode = 0;
    private int baseCode = 0;

    //region Constructors
    public OPCode(int _variantCode, int _baseCode) throws Exception
    {
        if (_variantCode < 0 || _variantCode > (ASEConstants.MAX_OPCODES - 1))
        {
            throw new ASEException("Bad variant code.");
        }

        if (_baseCode < 0 || _baseCode > (ASEConstants.MAX_OPCODES - 1))
        {
            throw new ASEException("Bad base code.");
        }

        variantCode = _variantCode;
        baseCode = _baseCode;
    }

    public OPCode(int _baseCode) throws Exception
    {
        if (_baseCode < 0 || _baseCode > (ASEConstants.MAX_OPCODES - 1))
        {
            throw new ASEException("Bad base code.");
        }

        baseCode = _baseCode;
    }
    //endregion

    @Override
    public int getBaseCode()
    {
        return baseCode;
    }

    @Override
    public int getVariantCode()
    {
        return variantCode;
    }

    @Override
    public char convertToChar()
    {
        return ByteTools.buildChar((byte)variantCode, (byte)baseCode);
    }
}
