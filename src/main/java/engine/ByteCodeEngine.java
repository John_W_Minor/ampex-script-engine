package engine;

import com.ampex.amperabase.ITransAPI;
import engine.binary.IBinary;
import engine.data.*;
import engine.exceptions.ASEException;
import engine.exceptions.ASERuntimeException;
import engine.operators.IOperator;
import engine.operators.reserved_set.*;
import engine.operators.reserved_set.word8v1.IOperatorReservedWord8;
import engine.program.IOPCode;
import engine.program.IProgram;
import logging.IAmpexLogger;
import org.reflections.Reflections;

import java.util.ArrayList;
import java.util.Set;
import java.util.Stack;

/**
 * Created by Queue on 3/7/2018.
 */
public class ByteCodeEngine implements IByteCodeEngine
{
    //region Member Fields
    private int version = 0;
    private boolean word8 = true;

    private IOperator[] operators = new IOperator[ASEConstants.MAX_OPCODES];

    private boolean opSetComplete = false;
    //endregion

    //region Constructors
    public ByteCodeEngine(int _version)
    {
        version = _version;
        word8 = true;

        if (version == 1)
        {
            word8 = true;
        }
    }

    @Deprecated
    public ByteCodeEngine(int _version, boolean _word8)
    {
        version = _version;
        word8 = _word8;
    }
    //endregion

    //region Instruction Set
    @Override
    public void addReservedWord8Operators()
    {
        addOperators("engine.operators.reserved_set.word8v1", IOperatorReservedWord8.class);
    }

    @Override
    public void addOperators(String _package)
    {
        addOperators(_package, IOperator.class);
    }

    @Override
    public void addOperators(String _package, Class _interface)
    {
        Reflections reflections = new Reflections(_package);

        Set<Class<? extends Object>> operatorClasses = reflections.getSubTypesOf(_interface);

        for (Class operatorClass : operatorClasses)
        {
            addOperator(operatorClass);
        }
    }

    @Override
    public void addOperator(Class iOperatorClass)
    {
        if (opSetComplete)
        {
            throw new ASERuntimeException("Program tried to register an operator.", version);
        }

        Object typeCheckObject = null;

        try
        {
            typeCheckObject = iOperatorClass.newInstance();
            if (!IOperator.class.isInstance(typeCheckObject))
            {
                throw new ASERuntimeException("Bad operator registered.", version);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            throw new ASERuntimeException("Bad operator registered.", version);
        }

        IOperator operator = (IOperator) typeCheckObject;

        int baseCode = operator.getBaseCode();

        if (baseCode < 0 || baseCode > (ASEConstants.MAX_OPCODES))
        {
            throw new ASERuntimeException("Program tried to register with a bad base code: " + baseCode, version);
        }

        if (operator.getKeyword() == null)
        {
            throw new ASERuntimeException("Program tried to register the " + operator.getClass().getName() + " operator, but it had a null keyword.", version);
        }

        if (baseCode < ASEConstants.RESERVED_OPCODES && !IOperatorReserved.class.isInstance(operator))
        {
            throw new ASERuntimeException("Program tried to register the " + operator.getClass().getName() + " operator with a reserved base code: " + baseCode, version);
        }

        if (operators[baseCode] != null)
        {
            throw new ASERuntimeException("The " + operators[baseCode].getKeyword() + " operator is already registered to base code: " + baseCode, version);
        }

        operators[baseCode] = operator;

        int a = 0;
    }

    @Override
    public void finalizeOperators()
    {
        opSetComplete = true;
    }

    @Override
    public IOperator[] getOperators()
    {
        return operators.clone();
    }
    //endregion

    //region Execution
    private void executeOPCode(Stack<IDataElement> _stack, IBinary _binary, IProgram _program, IConstantMemory _constantMemory, IJumpMemory _jumpMemory, IWritableMemory _writableMemory, IOPCode _opCode, ITransAPI _transaction, byte[] _executionAddress) throws Exception
    {
        IOperator operator = operators[_opCode.getBaseCode()];

        if (operator == null)
        {
            throw new ASEException("No operator registered for opcode " + _opCode.getBaseCode() + ".", version);
        }

        operator.execute(_stack, _binary, _program, _constantMemory, _jumpMemory, _writableMemory, _opCode, _transaction, _executionAddress.clone());
    }

    @Override
    public ArrayList<IDataElement> executeProgram(IBinary _binToExecute, IWritableMemory _writableMemory, ITransAPI _transaction, byte[] _executionAddress, boolean _debug) throws Exception
    {
        if (_binToExecute == null)
        {
            throw new ASEException("Null _binToExecute argument");
        }

        if (_binToExecute.getBCEVersion() != version)
        {
            throw new ASEException("Binary version did not match engine version.");
        }

        if (_writableMemory == null)
        {
            throw new ASEException("Null _writableMemory argument");
        }

        if (!opSetComplete)
        {
            throw new ASEException("Execution began before the operation set was declared complete.", version);
        }

        if (_binToExecute.isWord8() != word8)
        {
            throw new ASEException("Binary is word16, but this ByteCodeEngine object is word8", version);
        }

        IAmpexLogger logger = ASELogging.getLogger();

        int[] operationsCounter = new int[ASEConstants.MAX_OPCODES];

        boolean terminate = false;

        IProgram program = _binToExecute.getProgram().copyProgram();
        IConstantMemory constantMemory = _binToExecute.getConstantMemory();
        IJumpMemory jumpMemory = _binToExecute.getJumpMemory();
        IWritableMemory writableMemory = _writableMemory.copyWritableMemory();
        byte[] executionAddress = _executionAddress.clone();

        if (!word8)
        {
            program.word16Mode();
        }

        Stack<IDataElement> stack = new Stack<>();

        while (true)
        {
            try
            {
                IOPCode opCode = program.getNextWord();

                if (opCode == null)
                {
                    if (_debug)
                    {
                        if (logger != null)
                        {
                            if(program.isSealed())
                            {
                                logger.debug("\nProgram terminating because the program counter has exceeded the size of the program.");
                            } else
                            {
                                logger.debug("\nProgram terminating because the program was not sealed.");
                            }
                        }
                    }
                    break;
                }

                int baseCode = opCode.getBaseCode();

                if (baseCode == ReservedOperatorsConstants.TERMINATE)
                {
                    terminate = true;
                    if (_debug)
                    {
                        if (logger != null)
                        {
                            logger.debug("\nProgram terminating.");
                        }
                    }
                }

                operationsCounter[baseCode]++;

                IOperator operator = operators[baseCode];

                if (operator == null)
                {
                    throw new ASEException("No operator registered for opcode " + baseCode + ".", version);
                }

                if (_debug)
                {
                    String debug = "\nProgram counter is " + program.getProgramCounter() + ". ";

                    if (program.isWord8())
                    {
                        debug = debug + "\nOpcode is " + opCode.getBaseCode() + "." + "Keyword is " + operator.getKeyword() + ".";
                    } else
                    {
                        debug = debug + "\nOpcode is " + opCode.getVariantCode() + "." + opCode.getBaseCode() + "." + " Keyword is " + operator.getKeyword() + ".";
                    }

                    if (logger != null)
                    {
                        logger.debug(debug);
                    }
                }

                int maxExecutions = operator.getMaxExecutions();

                if (maxExecutions != -1 && operationsCounter[baseCode] > maxExecutions)
                {
                    throw new ASEException("Execution limit of Opcode " + baseCode + " has been exceeded.", version);
                }

                executeOPCode(stack, _binToExecute, program, constantMemory, jumpMemory, writableMemory, opCode, _transaction, executionAddress);

                if (stack.size() > ASEConstants.STACK_MAX_DEPTH)
                {
                    throw new ASEException("Stack overflow.", version);
                }

                if (terminate)
                {
                    break;
                }

            } catch (Exception e)
            {
                if (_debug)
                {
                    e.printStackTrace();

                    ArrayList<IDataElement> result = new ArrayList<>();
                    result.addAll(stack);

                    return result;
                } else
                {
                    throw e;
                }
            }
        }

        ArrayList<IDataElement> result = new ArrayList<>();
        result.addAll(stack);

        return result;
    }
    //endregion

    @Override
    public int getVersion()
    {
        return version;
    }

    @Override
    public boolean isWord8()
    {
        return word8;
    }
}
