package engine.exceptions;

import engine.ASELogging;
import logging.IAmpexLogger;

public class ASEException extends Exception
{
    public ASEException(String _message)
    {
        super(_message);
    }

    public ASEException(String _message, int _version)
    {
        super(generateMessage(_message, _version));
    }

    private static String generateMessage(String _message, int _version)
    {
        return _message + "\nError found on Byte Code Engine version " + _version + ".";
    }

    @Override
    public void printStackTrace()
    {
        IAmpexLogger logger = ASELogging.getLogger();
        if(logger != null)
        {
            logger.error("", this);
        }
    }
}
