package engine.exceptions;

import engine.ASELogging;
import logging.IAmpexLogger;

public class ASERuntimeException extends RuntimeException
{
    public ASERuntimeException(String _message)
    {
        super(_message);
    }

    public ASERuntimeException(String _message, int _version)
    {
        super(generateMessage(_message, _version));
    }

    private static String generateMessage(String _message, int _version)
    {
        return _message + "\nError found on Byte Code Engine version " + _version + ".";
    }

    @Override
    public void printStackTrace()
    {
        IAmpexLogger logger = ASELogging.getLogger();
        if(logger != null)
        {
            logger.error("", this);
        }
    }
}
